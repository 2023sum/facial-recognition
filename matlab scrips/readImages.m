% This function will read n images from a given path
% and return a 1:n cell where each item is a differnt read image
% (images must be numbered sequentually to be read)
%
% path - the dir path of the image with 
% numberOfImages - The number of images to read 
function outImages = readImages(path,numberOfImages)
    %init the return cell size
    outImages = cell(1,numberOfImages);
    
    %ini file path name vars
    filePattern = fullfile(path, '*.png');
    pngFiles = dir(filePattern);

    %Get n images and add them to the cell
    for i = 1:numberOfImages
      baseFileName = pngFiles(i).name;
      fullFileName = fullfile(path, baseFileName);
      outImages{i} = imread(fullFileName);
    end
end