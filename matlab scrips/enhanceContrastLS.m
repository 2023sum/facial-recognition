% This is a function to perfom a linear stretching operation on
% a collection of images
%
% images - The images to operate on
% c - The value to increase/decrease brightness by
% m - The value to increase/decrease contrast by
% showOutput - Boolean value to show a sample of before and after
%              opperation
%
% outImages - The colleciton of images with the opperation applied
function outImages = enhanceContrastLS(images,m,c,showOutput)
    %init return cell
    numImages = size(images);
    outImages = cell(1,numImages(2));

    %get the new contrast value from a LUT
    LutContrast = contrast_LS_LUT(m,c);

    %perform the opperation on all the images
    for i = 1:numImages(2)
        outImages{i} = intlut(images{i},LutContrast);
    end
    
    %if show output is on
    if(showOutput)
        %select a random sample
        rnd = randi(size(images),1);
       
        %display original sample vs new sample
        subplot(2,3,1)
        imshow(images{rnd}), title("Original image")
        subplot(2,3,4)
        histogram(images{rnd},'BinLimits',[0,256],'BinWidth',1), title("Original Image Hist")
        subplot(2,3,2)
        plot(0:255,LutContrast), title("LUT")
        subplot(2,3,3)
        imshow(outImages{rnd}), title("New image")
        subplot(2,3,6)
        histogram(outImages{rnd},'BinLimits',[0,256],'BinWidth',1), title("new Image Hist")
    end
    
end