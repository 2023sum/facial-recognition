function acc = KNN(features,labels,trainnum,testnum,replacement,k)

%get train features and labels
sampleSeed = RandStream('mlfg6331_64');
trainFeatures = datasample(sampleSeed,features,trainnum,Replace=replacement);
trainLabels = datasample(sampleSeed,labels,trainnum,Replace=replacement);

%get test features and labels
sampleSeed = RandStream('mlfg6331_64');
testFeatures = datasample(sampleSeed,features,testnum,Replace=replacement);
testLabels = datasample(sampleSeed,labels,testnum,Replace=replacement);

%train model
model = fitcknn(trainFeatures,trainLabels,NumNeighbors=k,Standardize=true);

%use test data to check accuracy of model
correctness = [];
for i = 1:testnum
    pred = predict(model,testFeatures(i));  
    if(strcmp(pred,testLabels(i)))
         correctness(i) = 1;
    else
        correctness(i) = 0;
    end
end

%calculate the accuracy of the model
total = 0;
for i = 1:testnum
    total = total + correctness(i);
end
 
acc = total/testnum;

end