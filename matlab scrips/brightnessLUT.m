%lookup table for brightness enhancement from prac-01
function Lut = brightnessLUT(c)

Lut = [0:255];

for i = 1:256
    if i < -c
        Lut(i) = 0;
    else if i > 255 - c
        Lut(i) = 255;
    else
        Lut(i) = Lut(i) + c;
    end
    end
end

Lut=uint8(Lut);
end