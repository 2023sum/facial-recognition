function Iout = enhanceContrastPL(Iin,gamma)

Iout= intlut(Iin,contrast_PL_LUT(gamma));


end