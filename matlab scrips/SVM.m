function acc = SVM(images,labels,images2,labels2)
    addpath  .\SVM-KM\

    %% training
    
    % facesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\face - Copy";
    % nonfacesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\non-face - Copy";
    % trainFaceNum = 69;
    % trainNonfaceNum = 55;
    % imagesFace = readImages(facesFolder,trainFaceNum);
    % imagesNonface = readImages(nonfacesFolder,trainNonfaceNum);
    % images = [imagesFace,imagesNonface];
    %
    % labelsFace = zeros(trainFaceNum,1); %0 represents face
    % labelsNonface = ones(trainNonfaceNum,1); %1 represents non-face
    % labels = vertcat(labelsFace,labelsNonface);
    
    for i=1:length(images)
        temp = cell2mat(images(i));
        outimages(:,:,i) = temp;
    end
    outimages = reshape(outimages, size(outimages, 1) * size(outimages, 2), size(outimages, 3));
    outimages = double(outimages) / 255;
    outimages = outimages(:,1:end)';
    images = outimages;
    
    
    %Display the first 100 images
    figure
    for i=1:100
        %Revert back to 27x18 image format
        Im = reshape(images(i,:),27,18);
        subplot(10,10,i), imshow(Im), title(['label: ',num2str(labels(i))])
    end
    
    [U,S,X_reduce] = pca(images,3);
    imean=mean(images,1);
    X_reduce=(images-ones(size(images,1),1)*imean)*U(:,1:3);
    
    figure, hold on
    colours= ['r.'; 'g.'; 'b.'; 'k.'; 'y.'; 'c.'; 'm.'; 'r+'; 'g+'; 'b+'; 'k+'; 'y+'; 'c+'; 'm+'];
    count=0;
    for i=min(labels):max(labels)
        count = count+1;
        indexes = find (labels == i);
        plot3(X_reduce(indexes,1),X_reduce(indexes,2),X_reduce(indexes,3),colours(count,:))
    end
    
    %Perform training
    modelSVM = SVMtraining(images, labels);
    
    % %After calculating the support vectors, we can draw them in the previous image
    % hold on
    % %transformation to the full image to the best 3 dimensions
    imean=mean(images,1);
    xsup_pca=(modelSVM.xsup-ones(size(modelSVM.xsup,1),1)*imean)*U(:,1:3);
    % plot support vectors
    h=plot3(xsup_pca(:,1),xsup_pca(:,2),xsup_pca(:,3),'go');
    set(h,'lineWidth',5)
    
    %% testing
    
    % % imagesFace2 = readImages(facesFolder,69);
    % % imagesNonface2 = readImages(nonfacesFolder,55);
    % % images2 = [imagesFace2,imagesNonface2];
    % %
    % % labelsFace2 = zeros(69,1); %0 represents face
    % % labelsNonface2 = ones(55,1); %1 represents non-face
    % % labels2 = vertcat(labelsFace2,labelsNonface2);
    %
    % testing = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\testing";
    % %TEST DATA
    % images2 = readImages(testing,10);
    % labels2 = [1;0;1;0;1;0;1;0;1;0];
    % %%%%%%%%%%%%
    
    for i=1:length(images2)
        temp = cell2mat(images2(i));
        outimages2(:,:,i) = temp;
    end
    outimages2 = reshape(outimages2, size(outimages2, 1) * size(outimages2, 2), size(outimages2, 3));
    outimages2 = double(outimages2) / 255;
    outimages2 = outimages2(:,1:end)';
    images2 = outimages2;
    
    for i=1:size(images2,1)
        testnumber= images2(i,:);
        classificationResult(i,1) = SVMTesting(testnumber,modelSVM);
    end
    
    
    %% Evaluation
    
    % Compare prediction to estimation
    comparison = (labels2==classificationResult);
    
    %Accuracy is the most common metric. It is defiend as the numebr of
    %correctly classified samples/ the total number of tested samples
    acc = sum(comparison)/length(comparison);
    
    %display up to 100 of the correctly classified images
    figure
    title('Correct Classification')
    count=0;
    i=1;
    while (count<100)&&(i<=length(comparison))
        if comparison(i)
            count=count+1;
            subplot(10,10,count)
            Im = reshape(images2(i,:),27,18);
            imshow(Im)
        end
        i=i+1;
    end
    
    %display up to 100 of the incorrectly classified images
    figure
    title('Wrong Classification')
    count=0;
    i=1;
    while (count<100)&&(i<=length(comparison))
        if ~comparison(i)
            count=count+1;
            subplot(10,10,count)
            Im = reshape(images2(i,:),27,18);
            imshow(Im)
            title(num2str(classificationResult(i)))
        end
        i=i+1;
    end
end

