function out = OtsuThresholding(image)
%ADDME Find the optimal threadhold value via Otsu's method

%Find number of gray levels in image
grayLevels = imhist(image);

[numGrayLevels, ~] = size(grayLevels);
% numGrayLevels = log2(numGrayLevels);

normalisedHistogram = grayLevels/sum(grayLevels);

rho = zeros(numGrayLevels);
for T = 1:numGrayLevels-1
    w1 = sum(normalisedHistogram(1:T));
    w2 = 1 - w1;
    
    mu1 = 0;
    for i = 1:T
        mu1 = mu1 + (i * normalisedHistogram(i));
    end
    mu1 = mu1 * (1/w1);

    mu2 = 0;
    for i = T+1:numGrayLevels-1
        mu2 = mu2 + (i * normalisedHistogram(i));
    end
    mu2 = mu2 * (1/w2);

    rho(T) = w1*w2 * (mu1-mu2)^2;
end

rho = rho';
rho = rho(1, :);

[~, threshold] = max(rho);

out = threshold;
end