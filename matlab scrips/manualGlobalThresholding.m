% This function perfom segmentation on all the images passed in
% it uses the manual global threshold approach 
%
% images - the images to segment
% t - The threshold value for segmentation
% type - 1 for segmenting light objects, 2 for segmenting dark objects
% showOutput - A boolean value to show a sample of the segmented images 
% num - the number of samples to show
%
% outImages - a collection of images that have been segmented

function outImages = manualGlobalThresholding(images,t,type,showOutput,num)
    %init return cell
    numImages = size(images);
    outImages = cell(1,numImages(2));

    %are we thresholding dark or light?
    %light
    %for each image in the input
    for i = 1:numImages(2)
        %get the image size
        imgSize = size(images{i});
        %set the output
        outImages{i} = images{i};
        
        %light
        if(type == 1)
            %for every pixel in the image
            for height = 1:imgSize(2)
                for width = 1:imgSize(1)
                    %if greater than threshold make white
                    if(images{i}(width,height) >= t)
                           outImages{i}(width,height) = 255;
                    %if less than threshold make white
                    else
                           outImages{i}(width,height) = 0;
                    end
                end
            end 
        elseif(type == 2)
            %for every pixel in the image
            for height = 1:imgSize(2)
                for width = 1:imgSize(1)
                    %if less than threshold make white
                    if(images{i}(width,height) <= t)
                           outImages{i}(width,height) = 255;
                    %if greater than threshold make black      
                    else
                           outImages{i}(width,height) = 0;
                    end
                end
            end
        end
    end
          
    %if show output is on show num random images
    if(showOutput)      
        %for the amount of images to show
        for i = 1:num
            %select a random image from the outImages
            rnd = randi(size(images),1);
            %display it
            subplot(1,num,i)
            imshow(outImages{i})
        end
    end

end