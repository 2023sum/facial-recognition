function outImages = histEqual(images)

% This is a function to perfom histogram equalisation on
% a collection of images
%
% images - The images to operate on
%
% outImages - The colleciton of images with the operation applied

    %init return cell
    numImages = length(images);
    outImages = cell(1,numImages);

    %perform the opperation on all the images
    for i = 1:numImages
        outImages{i} = histeq(images{i});
    end
end

