%lookup table for LS contrast enhancement from prac-01
function Lut = contrast_LS_LUT(m,c)

Lut = 0:255;

for i = 1:256
    if i < -c/m
        Lut(i) = 0;
    else if i > (255 - c)/m
        Lut(i) = 255;
    else
        Lut(i) = (m * Lut(i)) + c;
    end
    end
end

Lut=uint8(Lut);
end