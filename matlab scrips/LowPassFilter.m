function out = LowPassFilter(img, n, sharpen)
%ADDME Perform Noise Reduction on an image based on a userdefined mask size
%   out = ADDME(img, n) reduce noise and DO NOT resharpen
%   out = ADDME(img, n, sharpen) reduce noise and resharpen

    % assign default parameter to not resharpen image
    if nargin > 2
        sharpen = true;
    else
        sharpen = false;
    end

    % create a mask of size n x n
    % sum of values in mask = 1
    filterMask = ones(n, n)/n^2;

    % Apply a 2D filter to the image using the filter mask
    nrImage = filter2(filterMask, img);
    
    % Code To Resharpen Image
    if sharpen
        % New image has been blurred
        % Apply Sharpening to new image
        out = imsharpen(nrImage);
    else
        out = nrImage;
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% testImage = imread("D:\Users\Adam\Documents\CSC3067-2223-G36\provided material\im4.jpg");
% testImage = imnoise(testImage, "gaussian");
% noiseReducedImg = LowPassFilter(testImage, 3)
% 
% colormap("gray")
% subplot(2,2,1)
% imshow(testImage), title("Input Image")
% subplot(2,2,2)
% histogram(testImage,'BinLimits',[0,256],'BinWidth',1), title("Input Histogram")
% subplot(2,2,3)
% imagesc(noiseReducedImg), title("Noise Reduced Image")
% subplot(2,2,4)
% histogram(noiseReducedImg,'BinLimits',[0,256],'BinWidth',1), title("Noise Reduced Histogram")