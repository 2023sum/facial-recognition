clear all;
close all;

%load the faces
facesFolder = 'C:\Users\oisin\Desktop\Uni\4th year\Video Analytics and Machine Learning\Assignment\CSC3067-2223-G36\provided Material\images\face';
faces = readImages(facesFolder,69);

%load the non-faces
nonFacesFolder = 'C:\Users\oisin\Desktop\Uni\4th year\Video Analytics and Machine Learning\Assignment\CSC3067-2223-G36\provided Material\images\non-face';
nonFaces = readImages(nonFacesFolder,55);

%pre-processing
  
%turn images into feature vectors
features = [];
labels = cell(124,1);
for i = 1:124
    if(i <= 69)
        features(i,:) = gabor_feature_vector(faces{i});
        labels{i,1} = 'Face';
    else 
        j = i-69;
        features(i,:) = gabor_feature_vector(faces{j});
        labels{i,1} = 'Non-Face';
    end
end





x = [-100,-80,-60,-40,-20,0,20,40,60,80,100];
plot(x,acc), title("Affect on Accuracy when changing brightness where K=18 and Test:Train = 60:40 (With replacement)"),xlabel("c (change in overall brightness)"),ylabel("Acc")


%optomise for k(1-20) with a 80:20 train test ratio
% y1 = [];
% for i = 1:20
%     y1(i) = KNN(features,labels,99,25,true,i);
% end
% 
% x1 = [1:20];
% 
% plot(x,y1), title("How K affects model accuracy (With replacement)"), xlabel("K"),ylabel("Acc")

% %optomise train:test with k=1:20
% %20:80
% y2 = [];
% for i = 1:20
%     y2(i) = KNN(features,labels,25,99,false,i);
% end
% 
% %30:70
% y3 = [];
% for i = 1:20
%     y3(i) = KNN(features,labels,37,87,false,i);
% end
% 
% %40:60
% y4 = [];
% for i = 1:20
%     y4(i) = KNN(features,labels,50,74,false,i);
% end
% 
% %50:50
% y5 = [];
% for i = 1:20
%     y5(i) = KNN(features,labels,62,62,false,i);
% end
% 
% %60:40
% y6 = [];
% for i = 1:20
%     y6(i) = KNN(features,labels,74,50,false,i);
% end
% 
% %70:30
% y7 = [];
% for i = 1:20
%     y7(i) = KNN(features,labels,87,37,false,i);
% end
% 
% %80:20
% y8 = [];
% for i = 1:20
%     y8(i) = KNN(features,labels,99,25,false,i);
% end
% 
% 
% x2 = [1:20];

% plot(x2,y2,"red"), title("Test to train ratio with k=1:20 (with replacement)"), xlabel("K"),ylabel("Acc");
% hold on
% plot(x2,y3,"blue");
% hold on
% plot(x2,y4,"green");
% hold on
% plot(x2,y5,"cyan");
% hold on
% plot(x2,y6,"magenta");
% hold on
% plot(x2,y7,"yellow");
% hold on
% plot(x2,y8,"black");
% legend('20:80','30:70','40:60','50:50','60:40','70:30','80:20');

% %get average acc over K for more test:train and visa versa
% 
% %more test
% yavg1 = [];
% for i = 1:20
%     yavg1(i) = (y2(i) + y3(i) + y4(i))/3;
% end
% 
% %more train
% yavg2 = []
% for i = 1:20
%     yavg2(i) = (y6(i) + y7(i) + y8(i))/3
% end
% 
% plot(x2,yavg1,"red"), title("Avg Acc based on train:test ratio"), xlabel("K"),ylabel("Acc");
% hold on
% plot(x2,y5,"black");
% hold on
% plot(x2,yavg2,"blue");
% legend('More testing data','50:50','More training data');