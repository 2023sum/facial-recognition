clear all
close all

load fisheriris;

nTrees = 500;

%load the faces
facesFolder = 'C:\Users\oisin\Desktop\Uni\4th year\Video Analytics and Machine Learning\Assignment\CSC3067-2223-G36\provided Material\images\face';
faces = readImages(facesFolder,69);

%load the non-faces
nonFacesFolder = 'C:\Users\oisin\Desktop\Uni\4th year\Video Analytics and Machine Learning\Assignment\CSC3067-2223-G36\provided Material\images\non-face';
nonFaces = readImages(nonFacesFolder,55);