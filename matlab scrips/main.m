close all;
clear all;


%This will be the 'main' script where we can pull all our independant
%functions together to perform the full ML pipeline

%facesFolder = 'C:\Users\oisin\Desktop\Uni\4th year\Video Analytics and Machine Learning\Assignment\CSC3067-2223-G36\provided Material\images\face';
%im using boat as a test to make sure all the operations work as expected
%in the boat folder there is a single png "1.png" that is of the boat
%provided in practical 1
%boat = 'C:\Users\oisin\Desktop\boat';


%TRAIN DATA%
% with test data
% facesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\face";
% nonfacesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\non-face";
% trainFaceNum = 69;
% trainNonfaceNum = 55;
% without test data
%facesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\face - Copy";
%nonfacesFolder = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\non-face - Copy";
%trainFaceNum = 64;
%trainNonfaceNum = 50;
%imagesFace = readImages(facesFolder,trainFaceNum);
%imagesNonface = readImages(nonfacesFolder,trainNonfaceNum);
%images = [imagesFace,imagesNonface];

%labelsFace = zeros(trainFaceNum,1); %0 represents face
%labelsNonface = ones(trainNonfaceNum,1); %1 represents non-face
%labels = vertcat(labelsFace,labelsNonface);
%%%%%%%%%%%%

%TEST DATA%
%first five images from face and non-face folders
%testing = "C:\Users\acreg\OneDrive - Queen's University Belfast\Documents\MATLAB\FinalProject\provided material\images\testing";
%images2 = readImages(testing,10);
%labels2 = [1;0;1;0;1;0;1;0;1;0];
%%%%%%%%%%%


%LinearPointOpperation(images,40,true);
%enhanceContrastLS(images,.78,30,true);
%manualGlobalThresholding(images,80,2,true,6);
% images = powerLaw(images);
% figure
% montage(images);

load fisheriris;
acc = RF(meas,species,100,50,600,false);
 

%acc = SVM(images,labels,images2,labels2);
%disp("Accuracy = "+acc)

