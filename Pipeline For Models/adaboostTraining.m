function [influence, f] = adaboostTraining(trainSet, T)
    %%An implementation an adaboost training function%%
    % TrainSet is a set of image features by a series of images
    % T is the number of boosting rounds
    labels = trainSet(:,end);
    [numImages, numFeatures] = size(trainSet);
    numFeatures = numFeatures - 1;
    trainSet = trainSet(:,1:end-1);
    f = [];
    influence = [];
    
    %Store a list of the correct results
     for label = 1:size(labels)
         if labels(label) == 0
             labels(label) = -1;
         end
     end

    %For each boosting round
    for t = 1:T
        %For each feature in an image
        for features = 1:numFeatures
            featureSet = trainSet(:, features);
            % Create a set of normalised weights
            weights = ones(1, numImages)/numImages;
            trainingError = 0;

            % Select a weak classifier as a value within the feature set
            minVal = min(featureSet);
            maxVal = max(featureSet);
            f(t, features) = minVal + rand*(maxVal/2-minVal);
            % if the classification is wrong, increase trainingError
            for i = 1:numImages
                if featureSet(i) <= f(t, features) && labels(i) == -1
                    trainingError = trainingError + weights(i);
                elseif featureSet(i) >= f(t, features) && labels(i) == 1
                    trainingError = trainingError + weights(i);
                end
            end
            % Prevent overflow errors, if training error > 0.5: alpha < 0
            if trainingError < 0.5
                influence(t, features) = (log((1-trainingError)/(trainingError)))/2;
            else
                influence(t ,features) = 0;
            end
            % Readjust the weights
            for w = 1:numImages
                if featureSet(i) <= f(t, features) && labels(i) == -1
                    weights(w) = weights(w)*exp(influence(t, features));
                elseif featureSet(i) >= f(t) && labels(i) == 1
                    weights(w) = weights(w)*exp(influence(t, features));
                else
                    weights(w) = weights(w)*exp(influence(t, features)*-1);
                end
            end
        end
    end
end
