function void = findFaces(image,model,maskW,maskH,t)

    %get the size of the image
    [im_height, im_width] = size(image);
        
    disp("Extracting sub-Images")
    tic
    %get all of the "sub-images" from the main image
    subImages = {};
    index = 0;
    %for every possible mask position
    for h = 1: im_height - maskH
        for w = 1: im_width - maskW
            %Add a sub-image into the cell
            index = index + 1;
            subImages{h,w} =  image(h:(h + maskH)-1, w:(w + maskW)-1); 
        end
    end
    
    %pass each subimage into the classifier and if it thinks its a face put a 1
    %in the x,y position of where the top left of the mask was

    %The size of the sub images
    [ch, cw] = size(subImages);  

    %A matix with 1's where the classifier thinks there is a face
    prediction = [];  

    %A matrix in the same format as prediction[] but instead of a 1 it has the 
    % confidence that the model has with its selection as a probability
    probabilities = zeros(ch,cw); 
    toc
    disp("Classifying Sub-Images")
    tic

    %for each subimage perform classification and populate the above
    %matricies (run in parallel to save time)
    for i = 1:ch
        parfor j = 1:cw
            %perform feature extraction with gabor
            temp = gabor_feature_vector(subImages{i,j});
            %get the models prediction and confidence
            [pred, confidence] = predict(model, temp(1,:));
            %if the model predicted "face"
            if(pred == 1)
                %if the confidence is bigger than our threshold fill the
                %relevent array
                if(confidence(1,2) > t)
                    prediction(i,j) = pred;
                    probabilities(i,j) = confidence(1,2);
                %otherwise set it to 0
                else
                    prediction(i,j) = 0
                end
            else
                prediction(i,j) = 0
            end
        end
    end
    toc
    disp("Calculating regions")
    tic
    %calculate connected regions
    connectedRegions = bwconncomp(prediction,4);
    L4 = labelmatrix(connectedRegions);
    RGB_label = label2rgb(L4,'spring',"c","shuffle");
    
    %get how many regions there are and list them in the numPres array
    numsPres = [];
    for i = 1:ch
        for j = 1:cw
            if(ismember(L4(i,j),numsPres(:)))
                
            else
                numsPres(end + 1) = L4(i,j);
            end
        end
    end
    %remove 0 as a region and sort the array
    regions = numsPres(find(numsPres~=0));
    sort(regions);
    [~,s] = size(regions);
    toc
    disp("Getting best prediction for each region")
    tic
    %go through all the regions and get the best prediction from each, save
    %the x,y position of where the best prediction was into best[]
    best = [];
    for x = 1:s
        bestPrediction = 0;
        for i = 1:ch
            for j = 1:cw
                if(L4(i,j) == x)
                    %is this the best prob for this region?
                    if(probabilities(i,j) > bestPrediction)
                        bestPrediction = probabilities(i,j);
                        best(x,1) = i;
                        best(x,2) = j;
                    end
                end
            end
        end
    end
    toc
    disp("Show results")
    
    %show the original imahe
    subplot(2,2,1)
    imshow(image);
    hold on
    %Show the image with detection rectangles
    subplot(2,2,2)
    imshow(image);
    hold on
    %draw the rectangle from the best predictions array
    for i = 1:s
        yPos = best(i,1);
        xPos = best(i,2);
        rectangle('position',[xPos, yPos, maskW, maskH], 'EdgeColor','r');          
    end
    hold on
    %show the binarised regions
    subplot(2,2,3)
    imshow(prediction)
    hold on
    %show the regions with colours
    subplot(2,2,4)
    imshow(RGB_label)


end

