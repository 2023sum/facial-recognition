function outImages = powerLaw(images)

% This is a function to perfom a power's law operation on
% a collection of images
%
% images - The images to operate on
%
% outImages - The colleciton of images with the opperation applied

    %init return cell
    numImages = length(images);
    outImages = cell(1,numImages);

    %perform the opperation on all the images
    for i = 1:numImages

        %get most common colour
        mostOccur = mode(images{i},'all');
        if mostOccur <50
          gamma  = 0.5;
        end
        if (mostOccur>50) && (mostOccur<=100)
          gamma  = 0.75;
        end
        if (mostOccur>100) && (mostOccur<=150)
          gamma  = 1;
        end
        if (mostOccur>150) && (mostOccur<=200)
          gamma  = 1.25;
        end
        if mostOccur>200
          gamma  = 1.5;
        end
        outImages{i} = enhanceContrastPL(images{i},gamma);

%      colormap("gray");
%      subplot(2,3,1);
%      image(images{i});
%      subplot(2,3,4);
%      H = histogram(images{i},'BinLimits',[0 256],'BinWidth',1);
%      h = H.Values;
%      subplot(2,3,2);
%      plot(contrast_PL_LUT(gamma));
%      subplot(2,3,3);
%      image(outImages{i});
%      subplot(2,3,6);
%      histogram(outImages{i},'BinLimits',[0 256],'BinWidth',1);

    end
end