close all
clear all

%set seed
rng(1)

%% Load Dataset %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%load the training images
FacesFolder = './images/face';
NonFacesFolder = './images/non-face';

Faces = readImages(FacesFolder,69);
NonFaces = readImages(NonFacesFolder,55);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% pre-processing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Power law 

Faces = powerLaw(Faces);
NonFaces = powerLaw(NonFaces);


%% Histogram equilisation 

Faces = histEqual(Faces);
NonFaces = histEqual(NonFaces);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Create feature and label vectors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Using 1 and 0 for labels, where:
%   1 = Face
%   0 = Non-Face

%% whole images 

Features = [];
Labels = [];
for i = 1:124
    if(i <= 69)
        Features(i,:) = Faces{i}(:)';
        Labels(i,:) = 1; 
    else
        j = i-69;
        Features(i,:) = NonFaces{j}(:)';
        Labels(i,:) = 0;
    end
end

%% Gabor features

Features = [];
Labels = [];
for i = 1:124
    if(i <= 69)
        Features(i,:) = gabor_feature_vector(Faces{i});
        Labels(i,:) = 1; 
    else
        j = i-69;
        Features(i,:) = gabor_feature_vector(NonFaces{j});
        Labels(i,:) = 0; 
    end
end

%% Hog fetures
Features = [];
Labels = [];
for i = 1:124
    if(i <= 69)
        Features(i,:) = hog_feature_vector(Faces{i});
        Labels(i,:) = 1; 
    else
        j = i-69;
        Features(i,:) = hog_feature_vector(NonFaces{j});
        Labels(i,:) = 0; 
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Produce Train and test sets %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%shuffle the dataset
dataset = [Features, Labels];
dataset_shuffled = dataset(randperm(size(dataset,1)),:);

%% Selected ratio of Train:Test

%chosen split
trainNum = 99;

%get train and test set
Train = dataset_shuffled(1:trainNum,:);
Test = dataset_shuffled(trainNum+1:124,:);

%% Crosvalidation split
% Dataset split into 3 segments 41:41:42 

%train seg 1 and 2, test seg 3
Train1 = dataset_shuffled(1:82,:);
Test1 = dataset_shuffled(83:124,:);

%train seg 2 and 3, test seg 1
Train2 = dataset_shuffled(42:124,:);
Test2 = dataset_shuffled(1:41,:);

%train seg 1 and 3, test seg 2
intr1 = dataset_shuffled(1:41,:);
intr2 = dataset_shuffled(84:124,:);

Train3 = cat(1,intr1,intr2);
Test3 = dataset_shuffled(42:83,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Train And Test Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Straightforward selected split model

LengthOfVector = size(dataset);
numOfFeatures = LengthOfVector(2)-1;

%get train features and labels
feat = Train(1:trainNum,1:numOfFeatures);
lab = Train(1:trainNum,numOfFeatures+1);

%select k
k = 3;

%train model
sfModel = fitcknn(feat,lab,NumNeighbors=k,Standardize=true);

%get test features and labels
testFeat = Test(1:124-trainNum,1:numOfFeatures);
testLab = Test(1:124-trainNum,numOfFeatures+1);

%check how many times the model predicts correctly
TP = 0;
TN = 0;
FP = 0;
FN = 0;

for i = 1:124-trainNum
   pred = predict(sfModel,testFeat(i));
   %Correctly Identified
   if(pred == testLab(i))
       %TP
       if(pred == 1)
            TP = TP + 1;
       end
       %TN
       if(pred == 0)
            TN = TN + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP = FP + 1;
       end
       %FN
       if(pred == 0)
            FN = FN + 1;
       end
   end
end

%calculate the acc
Acc = (TN+TP)/(124-trainNum)

%% cross-validated model

k = 5;

LengthOfVector = size(dataset);
numOfFeatures = LengthOfVector(2)-1;

%train models
train1_feat = Train1(:,1:numOfFeatures);
train1_lab = Train1(:,numOfFeatures+1);

train2_feat = Train2(:,1:numOfFeatures);
train2_lab = Train2(:,numOfFeatures+1);

train3_feat = Train3(:,1:numOfFeatures);
train3_lab = Train3(:,numOfFeatures+1);

CvModel1 = fitcknn(train1_feat,train1_lab,NumNeighbors=k,Standardize=true);
CvModel2 = fitcknn(train2_feat,train2_lab,NumNeighbors=k,Standardize=true);
CvModel3 = fitcknn(train3_feat,train3_lab,NumNeighbors=k,Standardize=true);

%test models
test1_feat = Test1(:,1:numOfFeatures);
test1_lab = Test1(:,numOfFeatures+1);

test2_feat = Test2(:,1:numOfFeatures);
test2_lab = Test2(:,numOfFeatures+1);

test3_feat = Test3(:,1:numOfFeatures);
test3_lab = Test3(:,numOfFeatures+1);

TP = [0,0,0];
TN = [0,0,0];
FP = [0,0,0];
FN = [0,0,0];

%Test the models

%model 1
for i = 1:42
   pred = predict(CvModel1,test1_feat(i));
   %Correctly Identified
   if(pred == test1_lab(i))
       %TP
       if(pred == 1)
            TP(1) = TP(1) + 1;
       end
       %TN
       if(pred == 0)
            TN(1) = TN(1) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(1) = FP(1) + 1;
       end
       %FN
       if(pred == 0)
            FN(1) = FN(1) + 1;
       end
   end
end

%model 2
for i = 1:41
   pred = predict(CvModel2,test2_feat(i));
   %Correctly Identified
   if(pred == test2_lab(i))
       %TP
       if(pred == 1)
            TP(2) = TP(2) + 1;
       end
       %TN
       if(pred == 0)
            TN(2) = TN(2) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(2) = FP(2) + 1;
       end
       %FN
       if(pred == 0)
            FN(2) = FN(2) + 1;
       end
   end
end

%model 3
for i = 1:42
   pred = predict(CvModel3,test3_feat(i));
   %Correctly Identified
   if(pred == test3_lab(i))
       %TP
       if(pred == 1)
            TP(3) = TP(3) + 1;
       end
       %TN
       if(pred == 0)
            TN(3) = TN(3) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(3) = FP(3) + 1;
       end
       %FN
       if(pred == 0)
            FN(3) = FN(3) + 1;
       end
   end
end

%% SVM straightforward

LengthOfVector = size(dataset);
numOfFeatures = LengthOfVector(2)-1;

%get train features and labels
feat = Train(1:trainNum,1:numOfFeatures);
lab = Train(1:trainNum,numOfFeatures+1);

SVMmodel = fitcsvm(feat,lab,'Standardize',true,'ScoreTransform','logit')

%get test features and labels
testFeat = Test(1:124-trainNum,1:numOfFeatures);
testLab = Test(1:124-trainNum,numOfFeatures+1);

%check how many times the model predicts correctly
TP = 0;
TN = 0;
FP = 0;
FN = 0;

for i = 1:124-trainNum
   pred = predict(SVMmodel,testFeat(i,:));
   %Correctly Identified
   if(pred == testLab(i))
       %TP
       if(pred == 1)
            TP = TP + 1;
       end
       %TN
       if(pred == 0)
            TN = TN + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP = FP + 1;
       end
       %FN
       if(pred == 0)
            FN = FN + 1;
       end
   end
end

%calculate the acc
Acc = (TN+TP)/(124-trainNum)

%% cross-validated SVM

LengthOfVector = size(dataset);
numOfFeatures = LengthOfVector(2)-1;

%train models
train1_feat = Train1(:,1:numOfFeatures);
train1_lab = Train1(:,numOfFeatures+1);

train2_feat = Train2(:,1:numOfFeatures);
train2_lab = Train2(:,numOfFeatures+1);

train3_feat = Train3(:,1:numOfFeatures);
train3_lab = Train3(:,numOfFeatures+1);

SVMmodel1 = fitcsvm(train1_feat,train1_lab,'Standardize',true)
SVMmodel2 = fitcsvm(train2_feat,train2_lab,'Standardize',true)
SVMmodel3 = fitcsvm(train3_feat,train3_lab,'Standardize',true)

%test models
test1_feat = Test1(:,1:numOfFeatures);
test1_lab = Test1(:,numOfFeatures+1);

test2_feat = Test2(:,1:numOfFeatures);
test2_lab = Test2(:,numOfFeatures+1);

test3_feat = Test3(:,1:numOfFeatures);
test3_lab = Test3(:,numOfFeatures+1);

TP = [0,0,0];
TN = [0,0,0];
FP = [0,0,0];
FN = [0,0,0];

%Test the models

%model 1
for i = 1:42
   pred = predict(SVMmodel1,test1_feat(i,:));
   %Correctly Identified
   if(pred == test1_lab(i))
       %TP
       if(pred == 1)
            TP(1) = TP(1) + 1;
       end
       %TN
       if(pred == 0)
            TN(1) = TN(1) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(1) = FP(1) + 1;
       end
       %FN
       if(pred == 0)
            FN(1) = FN(1) + 1;
       end
   end
end

%model 2
for i = 1:41
   pred = predict(SVMmodel2,test2_feat(i,:));
   %Correctly Identified
   if(pred == test2_lab(i))
       %TP
       if(pred == 1)
            TP(2) = TP(2) + 1;
       end
       %TN
       if(pred == 0)
            TN(2) = TN(2) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(2) = FP(2) + 1;
       end
       %FN
       if(pred == 0)
            FN(2) = FN(2) + 1;
       end
   end
end

%model 3
for i = 1:42
   pred = predict(SVMmodel3,test3_feat(i,:));
   %Correctly Identified
   if(pred == test3_lab(i))
       %TP
       if(pred == 1)
            TP(3) = TP(3) + 1;
       end
       %TN
       if(pred == 0)
            TN(3) = TN(3) + 1;
       end
   %Incorectly identified
   else
       %FP
       if(pred == 1)
            FP(3) = FP(3) + 1;
       end
       %FN
       if(pred == 0)
            FN(3) = FN(3) + 1;
       end
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Optimizations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% K for simple split
% optimizeK(Train,Test,20)

%k for corss-validated model
optimizeK(Train1,Test1,20)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Evaluations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Plot ROC curves of the CV models
[~,Scores1] = predict(CvModel1,test1_feat);
rocObj1 = rocmetrics(test1_lab,Scores1,CvModel1.ClassNames);
[~,Scores2] = predict(CvModel2,test2_feat);
rocObj2 = rocmetrics(test2_lab,Scores2,CvModel2.ClassNames);
[~,Scores3] = predict(CvModel3,test3_feat);
rocObj3 = rocmetrics(test3_lab,Scores3,CvModel3.ClassNames);

%plot(rocObj1)
%hold on
plot(rocObj2)
%hold on 
%plot(rocObj3)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Sliding Window %%%%%%%%%%%%%%%

% maskW = 18;
% maskH = 27;
% 
% im1 = imread("images/group/im1.jpg");
% im2 = imread("images/group/im2.jpg");
% im3 = imread("images/group/im3.jpg");
% im4 = imread("images/group/im4.jpg");
% 
% 
% findFaces(im1,SVMmodel,maskW,maskH,0.7);






