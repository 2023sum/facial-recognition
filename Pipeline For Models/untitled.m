% Load the true class labels and predicted scores for each model
%load('data.mat');

% Compute the precision and recall for each model
for i = 1:5
    [precision{i}, recall{i}, ~] = precisionrecall(true_labels, predicted_scores(:,i));
end

% Plot the PR curve for each model
figure;
hold on;
for i = 1:5
    plot(recall{i}, precision{i});
end

% Add labels and legend
xlabel('Recall');
ylabel('Precision');
title('Precision-Recall Curve');
legend('Model 1', 'Model 2', 'Model 3', 'Model 4', 'Model 5');
