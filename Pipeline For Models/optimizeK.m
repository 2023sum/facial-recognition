function acc = optimizeK(Train,Test,toK)

%get train features and labels
numOfFeatures = size(Train);
feat = Train(:,1:numOfFeatures(2)-1);
lab = Train(:,numOfFeatures(2));

%get test features and labels
numOfTestFeatures = size(Test);
testfeat = Test(:,1:numOfTestFeatures(2)-1);
testlab = Test(:,numOfTestFeatures(2));

%optomise for k(1-20) with a 80:20 train test ratio
y = [];
for i = 1:toK

    %train model with K
    Model = fitcknn(feat,lab,NumNeighbors=i,Standardize=true);

    %use test data to check accuracy of model
    correctness = [];
    for j = 1:numOfTestFeatures(1)
        pred = predict(Model,testfeat(j));  
        if(pred == testlab(j))
            correctness(j) = 1;
        else
            correctness(j) = 0;
        end
    end

    %calculate the accuracy of the model
    total = 0;
    for k = 1:numOfTestFeatures(1)
        total = total + correctness(k);
    end

    y(i) = total/numOfTestFeatures(1);
end

%plot accuracy on graph
x = [1:toK];

plot(x,y), title("How K affects model accuracy"), xlabel("K"),ylabel("Acc")

acc = y;

end