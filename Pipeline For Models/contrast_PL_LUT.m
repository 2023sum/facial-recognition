%lookup table for LP contrast enhancement from prac-01
function Lut = contrast_PL_LUT(gamma)

Lut = 1:256;

for i = Lut

    Lut(i) = (Lut(i)^gamma)/(255^(gamma-1)) ;

    if Lut(i) >255
           Lut(i) = 255;
    elseif Lut(i) <0
           Lut(i) = 0; 
    end
end
Lut=uint8(Lut); 