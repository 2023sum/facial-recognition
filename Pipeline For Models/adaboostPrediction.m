function prediction = adaboostPrediction(f, influence, imageFeatures)
%Makes a prediction based on image features
%Using the results of the bootsing rounds, the f and alpha values
    predictions = [];
    [T, numFeatures] = size(influence);
    for feature = 1:numFeatures
        thisPrediction = 0;
        %Calculate the classified result for aeach boosting round
        for t = 1:T
            if imageFeatures(feature) >= f(t, feature)
                modifier = -1;
            else
                modifier = 1;
            end
            % The direction of the classifier is determined by the function
            % The influence determines how much of an affect this `T` value has on the prediction
            thisPrediction = thisPrediction + (influence(t, feature) * modifier);
        end
        % Convert the predictions to binary values
        % 1 for a face is present
        % 0 for a face is NOT present
        if thisPrediction < 0
            predictions(feature) = thisPrediction;
        else
            predictions(feature) = thisPrediction;
        end
        % The overall prediction is a majority of the predictions for each of the image features
        if sum(predictions) <= 0
            prediction = 0;
        else
            prediction = 1;
        end
    end
end
