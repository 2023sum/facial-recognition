import cv2
import sys

imagePath = sys.argv[1]
modelPath = sys.argv[2]

# Load the cascade
face_cascade = cv2.CascadeClassifier(modelPath)
# Read the input image
img = cv2.imread(imagePath)
# Convert into grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# Detect faces
faces = face_cascade.detectMultiScale(
    img,    #Image we are detecting from
    scaleFactor= 1.1,   #Acount for scale of faces
    minNeighbors = 2,  #for sliding window to say when to make a new box
    minSize = (2,2),  #min size each box can be
)
# Draw rectangle around the faces
for (x, y, w, h) in faces:
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
# Display the output
cv2.imshow('img', img)
cv2.waitKey()