Face Detection with KNN, SVM and Adaboost

1. Introduction
This project focuses on face detection using KNN, Support Vector Machines (SVM) and Adaboost algorithms.
Utilizes advanced techniques like Gabor feature extraction and sliding window approach for effective group image processing.
This project was created for an assignment in the Video Analytics and Machine Learning module in my final at QUB.

2. Features
Face Detection Algorithms:
Implementation of KNN, SVM and Adaboost algorithms for accurate face detection.
Integration of Gabor feature extraction for improved facial feature representation.

Image Processing Techniques:
Sliding window approach for efficient group image processing.
Support for various pre-processing techniques to enhance image quality and improve detection accuracy.

Result Analysis and Performance Metrics:
In-depth analysis of detection results.
Performance metrics such as accuracy, precision, and recall to evaluate the effectiveness of the algorithms.

3. Methodology
KNN, SVM and Adaboost Algorithms
Gabor Feature Extraction
Sliding Window Approach
Pre-processing Techniques

4. Installation
Clone the repository
Navigate to the project directory
Run from matlab scripts/main.m

6. Training the Model
Load dataset
Feature extraction
Train Model

7. Results
SVM using Gabor Features model
Sensitivity = 0.923
Precision = 0.946
